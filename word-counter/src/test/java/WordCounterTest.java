import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class WordCounterTest {

	private WordCounter wordCounter = new WordCounter();
	private static String longString;

	@BeforeClass
	public static void init() {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < 100_000; i++) stringBuffer.append(" word ");
		longString = stringBuffer.toString();
	}

	/* COUNT WORDS */

	@Test
	public void count_words_in_null_string() {
		assertTrue(wordCounter.countWords(null).isEmpty());
	}

	@Test
	public void count_words_in_empty_string() {
		String string = "";
		assertTrue(wordCounter.countWords(string).isEmpty());
	}

	@Test
	public void count_words_in_simple_string() {
		String string = "word";
		assertTrue(1 == wordCounter.countWords(string).get("word"));
	}

	@Test
	public void count_words_with_spaces_at_the_beginning_and_at_the_end() {
		String string = " word ";
		assertTrue(1 == wordCounter.countWords(string).get("word"));
	}

	@Test
	public void count_words_not_key_sensitive() {
		String string = "Word wOrd woRd worD word";
		assertTrue(5 == wordCounter.countWords(string).get("word"));
	}

	@Test
	public void count_words_with_strange_characters() {
		String string = "- Hello @alias7! Nice to meet you.\n" +
						"- Nice to meet you too.";
		assertTrue(null == wordCounter.countWords(string).get("word"));
		assertTrue(2 == wordCounter.countWords(string).get("nice"));
		assertTrue(2 == wordCounter.countWords(string).get("meet"));
		assertTrue(1 == wordCounter.countWords(string).get("alias7"));
	}

	@Test(timeout = 250)
	public void count_words_fast() {
		assertFalse(wordCounter.countWords(longString).isEmpty());
	}

	/* COUNT WORD */
	
	@Test
	public void count_word_null() {
		assertEquals(0, wordCounter.countWord("string", null));
	}

	@Test
	public void count_word_in_null_string() {
		assertEquals(0, wordCounter.countWord(null, "word"));
	}

	@Test
	public void count_word_in_string() {
		assertEquals(1, wordCounter.countWord("Hello World!", "world"));
	}

}
