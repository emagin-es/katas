import java.util.HashMap;
import java.util.Map;

public class WordCounter {

	public Map<String, Integer> countWords(String string) {
		Map<String, Integer> wordsCounted = new HashMap<>();

		if (string == null)
			return wordsCounted;

		String regex = "[^a-zA-Z0-9]+";
		String[] words = string.toLowerCase().split(regex);

		for (String word : words) {
			if (word.length() > 0) {
				if (wordsCounted.containsKey(word)) {
					wordsCounted.put(word, wordsCounted.get(word) + 1);
				} else {
					wordsCounted.put(word, 1);
				}
			}
		}

		return wordsCounted;
	}

	public int countWord(String string, String word) {
		if(string == null || word == null)
			return 0;

		String wordLowerCased = word.toLowerCase();
		Map<String, Integer> words = countWords(string);

		return words.containsKey(wordLowerCased) ? words.get(wordLowerCased) : 0;
	}

}
