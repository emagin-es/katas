import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CodeValidator {

	Map<Character, Character> charactersValids = Map.of(
			'{','}',
			'[',']',
			'(',')'
	);

	public boolean validate(String code) {
		boolean valid = true;
		List<Character> charsOpened =new ArrayList<>();

		for (Character character : code.toCharArray()) {
			boolean isOpenChar = charactersValids.containsKey(character);
			boolean isCloseChar = charactersValids.containsValue(character);

			if (isOpenChar) {
				charsOpened.add(character);
			} else if (isCloseChar) {
				int lastIndex = charsOpened.size() - 1;
				char lastOpenChar = charsOpened.get(lastIndex);
				boolean close = charactersValids.get(lastOpenChar).equals(character);

				if (close) {
					charsOpened.remove(lastIndex);
				} else {
					valid = false;
					break;
				}
			} else {
				throw new IllegalArgumentException("Character " + character + "invalid");
			}
		}

		return valid;
	}
}
