import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class CodeValidatorTest {
	@Test
	void validate_valid_code() {
		CodeValidator codeValidator = new CodeValidator();
		assertTrue(codeValidator.validate("[()]{}{[()()]()}"));
	}

	@Test
	void validate_invalid_code() {
		CodeValidator codeValidator = new CodeValidator();
		assertFalse(codeValidator.validate("[(])"));
	}
	
	@Test
	void validate_invalid_chars() {
		CodeValidator codeValidator = new CodeValidator();
		assertThrows(IllegalArgumentException.class, () -> {
			codeValidator.validate("!");
		});
	}
}
